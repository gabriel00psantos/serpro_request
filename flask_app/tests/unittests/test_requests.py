from flask import url_for
from .flask_base_tests_cases import TestFlaskBase


class TestCPFRequest(TestFlaskBase):
    def test_cpf_ok(self):
        self.create_user()
        token = self.create_token()
        response = self.client.get(url_for('serpro.index',
                                              cpf="63017285995"), headers=token)
        self.assertEqual(response.status_code, 200)

    def test_cpf_not_found(self):
        self.create_user()
        token = self.create_token()
        response = self.client.get(url_for('serpro.index',
                                              cpf="43467102808"), headers=token)
        self.assertEqual(response.status_code, 404)





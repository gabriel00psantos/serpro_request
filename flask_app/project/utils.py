import requests
import logging

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)


def make_request(cpf):
    authToken = '4e1a1858bdd584fdc077fb7d80f39283'
    headers = {
        'Authorization': 'Bearer ' + authToken
    }
    url = ('https://apigateway.serpro.gov.br/'
           'consulta-cpf-df-trial/v1/cpf/{}').format(cpf)

    possible_status = {
        "0": "regular",
        "2": "suspenso",
        "3": "falecido",
        "4": "irregular",
        "5": "cancelado",
        "8": "nulo",
        "9": "cancelado",
    }
    try:
        r = requests.get(url=url, headers=headers)
        extras = {
            'status': r.status_code,
            'raw_data': r.text,
            'cpf_consultado': cpf

        }
        logging.info('Requisição realizada - {}'.format(extras), extra=extras)
        if r.status_code == 404:
            logging.info('Nenhum registro encontrado', extra=extras)
            raise Exception('Nenhum registro encontrado.')

        data = r.json()

        if r.status_code == 200:
            logging.error('Sucesso ao realizar Requisição', extra=extras)
            situacao = possible_status.get(data['situacao']['codigo'])
            payload = {'status': situacao.lower()}
        else:
            mensagem_erro = data.get('mensagem', 'Erro desconhecido')
            logging.error('Erro ao realizar Requisição {}'.format(
                          mensagem_erro), extra=extras)
            payload = {'error': {'reason': mensagem_erro}}
    except Exception as e:
        payload = {'error': {'reason': str(e)}}

    return payload, r.status_code

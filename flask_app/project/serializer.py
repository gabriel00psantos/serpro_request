from marshmallow import fields, validates, ValidationError, post_load
from flask_marshmallow import Marshmallow
from project.model import User

ma = Marshmallow()


def configure(app):
    ma.init_app(app)

class LoginSchema(ma.Schema):
    class Meta:
        model = User

    username = fields.Str(required=True)
    password = fields.Str(required=True)
    email = fields.Str(required=True)

    @post_load
    def make_user(self, data, **kwargs):
        return User(**data)

class UserSchema(LoginSchema):
    class Meta:
        model = User
    
    email = fields.Str(required=False)

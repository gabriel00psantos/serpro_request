from flask import Blueprint, request
from project.utils import make_request
from flask_jwt_extended import jwt_required


bp_serpro_request = Blueprint('serpro', __name__)


@bp_serpro_request.route("/consulta/<cpf>", methods=['GET'])
@jwt_required
def index(cpf):
    data = make_request(cpf)
    return data
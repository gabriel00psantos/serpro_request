import logging
from flask import Blueprint, request, jsonify, current_app
from datetime import timedelta
from flask_jwt_extended import create_access_token, create_refresh_token
from project.model import User
from project.serializer import UserSchema, LoginSchema

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

bp_user = Blueprint('user', __name__)


@bp_user.route('/cadastro', methods=['POST'])
def register():
    try:
        us = UserSchema()
        user, error = us.load(request.json)
        if error:
            return {'error': {'reason': error}}, 401

        user.gen_hash()
        current_app.db.session.add(user)
        current_app.db.session.commit()
        return us.jsonify(user), 201
    except Exception as e:
        logging.error('Erro ao cadastrar Usuario {}'.format(str(e)))
        return {'error': {'reason': 'Erro ao cadastrar Usuario'}}, 500


@bp_user.route('/login', methods=['POST'])
def login():
    user, error = LoginSchema().load(request.json)

    if error:
        return {'error': {'reason': error}}, 401

    user = User.query.filter_by(username=user.username).first()

    if user and user.verify_password(request.json['password']):
        acess_token = create_access_token(
            identity=user.id,
            expires_delta=timedelta(days=1)
        )

        return jsonify({
            'acess_token': acess_token,
            'message': 'success'
        }), 200

    return jsonify({
        'message': 'Credenciais invalidas'
    }), 401

# Desafio prático back-end  - serpro_Request

Essa aplicação encapsula chamadas para api de consulta de CPF da serpro

Libs Utilizadas

- Flask
- flask_sqlalchemy
- flask_marshmallow
- marshmallow_sqlalchemy
- flask-jwt-extended


## Construa o ambiente

    docker-compose up --b --d

## Rode as migrations

	docker-compose exec web flask db init
	docker-compose exec web flask db migrate
	docker-compose exec web flask db upgrade

#### Rode os testes

	docker-compose exec web coverage run --source=project -m unittest discover -s tests/

#### Gerando relatórios de cobertura

    docker-compose exec web coverage html                                                           


# REST API

The REST API to the example app is described below.

## Cadastro de Usuario

### Request

`POST /cadastro/`

	curl --header "Content-Type: application/json" \
	--request POST \
	--data ' {"email": "mail@mail.com", "username": "gabriel", "password": "senha123"}' \
	http://localhost:5000/cadastro

### Response

	{
	  "email": "mail@mail.com", 
	  "password": "$pbkdf2-sha256$29000$1frfu5cS4vzfu3dujVHKOQ$ibfNLGJ17XMsbIGAv18Bd939kSDjP.5gMQ2Y/wImTlE", 
	  "username": "gabriel"
	}

## Login

### Request

`POST /login/`

	curl --header "Content-Type: application/json" \
	--request POST \
	--data ' {"username": "gabriel", "password": "senha123"}' \
	http://localhost:5000/login

### Response

	{
		"acess_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1ODkzNTcxMzEsIm5iZiI6MTU4OTM1NzEzMSwianRpIjoiN2JiOWE3OTktMTM1Zi00NjU0LWJhMWEtMDQ1NmExMmMzMWYwIiwiZXhwIjoxNTg5NDQzNTMxLCJpZGVudGl0eSI6MiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0.kCjiEC5O6SGfe4xqGUck5SVogecIdpRxLZryiHrXzxg",
		"message": "success"
	}

## Consulta

### Request

`POST /login/`

	curl -H 'Accept: application/json' -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1ODkzNTcxMzEsIm5iZiI6MTU4OTM1NzEzMSwianRpIjoiN2JiOWE3OTktMTM1Zi00NjU0LWJhMWEtMDQ1NmExMmMzMWYwIiwiZXhwIjoxNTg5NDQzNTMxLCJpZGVudGl0eSI6MiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0.kCjiEC5O6SGfe4xqGUck5SVogecIdpRxLZryiHrXzxg" http://localhost:5000/consulta/05137518743

### Response

	{
			"status": "falecido"
	}                                                                                                  

#### Execução no Postman

	Exportar arquivo "Consultas Serpro.postman_collection.json" no Postman e executar requisições

#### Utilizando interface para teste (utilizar seguinte repositório) 

	https://bitbucket.org/gabriel00psantos/serpro-request-front-end/src/master/